from django.conf.urls import url
from django.contrib.auth import views as auth_views
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls.static import static
from .import views

urlpatterns = [
    url(r'^$', views.HomePageOnload, name=''),
    url(r'^reg/$', views.RegistrationPageOnload, name='reg'),
    url(r'^login/$', views.LoginPage, name='login'), 
    url(r'^registeration_user_data/$', views.registeration_user_data, name='update_personnel_data'),
    url(r'^check_login_user_credentials/', views.check_login_user_credentials, name='check_login_user_credentials'),
    url(r'^logout/$', views.logout,name='logout'),
    url(r'^dashboard/$', views.DashboardPageOnload, name='dashboard'), 
    url(r'^date_time_value_function/(?P<data>[a-zA-Z0-9_#&%@*,():{}" ".|+-]+)$', views.date_time_value_function,name='date_time_value_function'),
    url(r'^languages/$', views.ApiCallPageOnload, name='languages'), 
    url(r'^paradigms/$', views.ApiCallPageOnload, name='paradigms'), 
    url(r'^programmers/$', views.ApiCallPageOnload, name='programmers'), 
    url(r'^api_submit_data/$', views.ApiSubmitData, name='api_submit_data'), 
    url(r'^get_api_list_view_data/(?P<data>[a-zA-Z0-9_#&%@*,():{}" ".|+-]+)$', views.get_api_list_view_data,name='date_time_value_function'),    
]   
# if settings.DEBUG:
#     urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
