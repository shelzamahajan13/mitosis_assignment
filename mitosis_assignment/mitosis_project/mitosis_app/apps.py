from django.apps import AppConfig


class MitosisAppConfig(AppConfig):
    name = 'mitosis_app'
