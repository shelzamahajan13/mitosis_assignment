from django.contrib import admin
from django.contrib import admin

from .models import UserRegisterationDetails,Paradigm,Language,Programmer

class UserRegisterationDetailsAdmin(admin.ModelAdmin):
	list_display = ('id','auth_user','first_name', 'last_name','email_mobile', 'password','user_registered_date')

	search_fields = ('id','auth_user','first_name', 'last_name','email_mobile','password','user_registered_date')


	row_id_fields=('auth_user',)
	
	def User(self, obj):
		return obj.auth_user.user

class ParadigmAdmin(admin.ModelAdmin):
	list_display = ('id','register_user_id','name')

	search_fields = ('id','register_user_id','name')


	row_id_fields=('register_user_id',)
	
	def UserRegister(self, obj):
		return obj.register_user_id.id

class LanguageAdmin(admin.ModelAdmin):
	list_display = ('id','register_user_id','paradigm','name')

	search_fields = ('id','register_user_id','paradigm','name')


	row_id_fields=('register_user_id','paradigm')
	
	def UserRegister(self, obj):
		return obj.register_user_id.id
	def paradigm(self,obj):
		return obj.paradigm.id

class ProgrammerAdmin(admin.ModelAdmin):
	list_display = ('id','register_user_id','name','languages')

	search_fields = ('id','register_user_id','name','languages')


	row_id_fields=('register_user_id',)
	
	def UserRegister(self, obj):
		return obj.register_user_id.id
	
	
	
admin.site.register(UserRegisterationDetails,UserRegisterationDetailsAdmin)
admin.site.register(Language,LanguageAdmin)
admin.site.register(Paradigm,ParadigmAdmin)
admin.site.register(Programmer,ProgrammerAdmin)
