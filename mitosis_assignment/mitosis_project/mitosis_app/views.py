from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.views.generic import TemplateView,ListView
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User

import logging
import requests
import json
import string,re
from django.contrib.auth import authenticate, login

import io
import sys
from django.utils import timezone
from datetime import datetime,timedelta
import datetime
from .models import UserRegisterationDetails,Paradigm,Language,Programmer

import os

import base64
import urllib.request
import urllib.parse

from django import template
from django.template.loader import get_template
from django.urls import resolve


stdlogger = logging.getLogger(__name__)

log = logging.getLogger(__name__)

if __name__ == '__main__':
	sys.exit(main())

domain_name='http://127.0.0.1:8000/'

# Create your views here.
def request_session_check(request):
	if 'user_id' in request.session:
		user_id = request.session['user_id']
		get_firstname=UserRegisterationDetails.objects.get(id=user_id).first_name
		get_lastname=UserRegisterationDetails.objects.get(id=user_id).last_name
		get_server_current_time= timezone.now()
		get_current_timestamp= get_server_current_time +datetime.timedelta(hours=5,minutes=30)
		get_format_current_date=str(get_current_timestamp)[:19]
		get_current_timestamp_new = datetime.datetime.strptime(get_format_current_date, '%Y-%m-%d %H:%M:%S')
		get_current_date=get_current_timestamp_new.date()
		get_month=get_current_date.strftime('%B') 
		day=get_current_date.day
		day_double=str(day).zfill(2)
		year=get_current_date.year
		get_current_date_value=str(day_double)+' '+str(get_month)+' '+str(year)
	
	else:
		user_id=''
		get_firstname=''
		get_lastname=''

	return get_firstname+'|'+get_lastname+'|'+str(user_id)+'|'+get_current_date_value

def RegistrationPageOnload(request):
	get_key = request.session.has_key('user_id')
	if get_key!=False:
		try:
			data=request_session_check(request);
			data=data.split('|');
			return render(request,'registration.html',{
				'get_firstname':data[0].title(),
				'get_lastname':data[1].title(),
				'user_id':data[2],
				
			})
		except Exception as ex:
			log.error("Error in redirecting page: %s" % ex)
	else:
		user_id=''
		return render(request,'registration.html',{"user_id":user_id})

def HomePageOnload(request):
	get_key = request.session.has_key('user_id')
	if get_key!=False:
		try:
			data=request_session_check(request);
			data=data.split('|');

			return render(request,'home.html',{

				'get_firstname':data[0].title(),
				'get_lastname':data[1].title(),
				'user_id':data[2],
				
			})

		except Exception as ex:
			log.error("Error in redirecting page: %s" % ex)
	else:
		user_id=''
		return render(request,'home.html',{"user_id":user_id})


def LoginPage(request):
	get_key = request.session.has_key('user_id')
	
	if get_key!=False:
		try:
			data=request_session_check(request);
			data=data.split('|');

			return render(request,'login.html',{

				'get_firstname':data[0].title(),
				'get_lastname':data[1].title(),
				'user_id':data[2],
				
			})

		except Exception as ex:
			log.error("Error in redirecting page: %s" % ex)
	else:
		user_id=''
		return render(request,'login.html',{"user_id":user_id})


@csrf_exempt
def registeration_user_data(request):
	result={}
	try:
		req_data=request.POST.get('data')
		req_data=json.loads(req_data)
		
	except Exception as ex:
		log.error("Error in redirecting page: %s" % ex)
	try:
		if request.method=="POST":
				
			cureent_register_timestamp= timezone.now()
			
			cureent_register_timestamp_new= cureent_register_timestamp +datetime.timedelta(hours=5,minutes=30)
			
			if not(User.objects.filter(username=req_data['email_mobile'])).exists():
				
				User.objects.create_user(req_data['email_mobile'],req_data['email_mobile'],req_data['password'])
				atuser = User.objects.get(username=req_data['email_mobile'])
				

				UserRegisterationDetails.objects.create(auth_user=atuser,
					first_name=req_data['fname'].strip(),
					last_name=req_data['last_name'].strip(),
					email_mobile=req_data['email_mobile'].strip(),
					password=req_data['password'].strip(),
					user_registered_date=cureent_register_timestamp_new,
					).save()
				result['status']='success'
			else:
				
				result['status']='exists'
					
		return HttpResponse(json.dumps(result), content_type='application/json')
	except Exception as ex:
		log.error("Error in redirecting page: %s" % ex)

@csrf_exempt
def check_login_user_credentials(request):
	login_result={}
	try:
		req_data=request.POST.get('data')
		req_data=json.loads(req_data)
		
	except Exception as ex:
		log.error("Error in redirecting page: %s" % ex)
	try:
		if request.method=="POST":
			
			if request.method=="POST":
				if User.objects.filter(username=req_data['email_mobile_login']).exists():
					user= authenticate(username=req_data['email_mobile_login'], password=req_data['password'])
					
					if(user!=None):
						login(request, user)
						current_user = request.user
						
						get_user_id = User.objects.get(username=req_data['email_mobile_login']).id
						
						if UserRegisterationDetails.objects.filter(auth_user=get_user_id).exists():
							
							user_id=UserRegisterationDetails.objects.get(auth_user=get_user_id).id
							
							if 'user_id' in request.session:
								request.session['user_id'] = user_id
							else:
								request.session['user_id'] = user_id

							
							login_result['user_id']=user_id
							
							login_result['status']='success'

						else:
						
							login_result['status']="wrong"
							
					else:
						
						login_result['status']="wrong"
				else:
					login_result['status']="email_mobile"
			
		return HttpResponse(json.dumps(login_result), content_type='application/json')
	except Exception as ex:
					log.error('Error in json data: %s' % ex) 

@csrf_exempt
def logout(request):
	try: 
		
		get_session_key=request.session.session_key
		request.session.flush()

		return redirect('http://'+request.get_host()+'/login')

	except Exception as ex: 
		log.error('Error in Logout API: req_data: %s' % ex)


@csrf_exempt
def DashboardPageOnload(request):
	try:
		
		get_key = request.session.has_key('user_id')
		
		if get_key!=False:
			data_get=request_session_check(request)
			data_values=data_get.split("|")
			array_set=['languages','paradigms','programmers']

			return render(request,'dashboard.html',{
				"get_firstname":data_values[0].title(),
				"get_lastname":data_values[1].title(),
				"current_date":data_values[3],
				"user_id":data_values[2],
				"array_set":array_set
			})
		else:
			 return redirect('http://'+request.get_host()+'/')
	except Exception as ex:
			log.error('Error in json data: %s' % ex)

@csrf_exempt
def date_time_value_function(request,data):
	result={}
	try:
		req_data=json.loads(data)
		
	except Exception as ex:
			log.error('Error in json data: %s' % ex)
	try:
		if request.method=='POST':
			
			if req_data['get_hours'] >= 4 and req_data['get_hours']<12:
				greeting_name    = "Good Morning"; 
			elif req_data['get_hours'] >= 12 and req_data['get_hours']<16:
				greeting_name   = "Good Afternoon";
			elif req_data['get_hours'] >= 16 and req_data['get_hours']<20: 
				greeting_name    = "Good Evening";
			elif req_data['get_hours'] >= 20 and req_data['get_hours']<4:
				greeting_name    = "Peaceful Night";
			else:
			   greeting_name="Be Relax - Be Happy"; 
			result['greeting_name']=greeting_name
		return HttpResponse(json.dumps(result), content_type='application/json')
	except Exception as ex:
		log.error("Error in redirecting page: %s" % ex)

@csrf_exempt
def ApiCallPageOnload(request):
	try:
		
		get_key = request.session.has_key('user_id')
		
		if get_key!=False:
			data_get=request_session_check(request)
			data_values=data_get.split("|")
			current_url = resolve(request.path_info).url_name
			
			if current_url=='paradigms':
				get_paradigm_value=''
				get_language_value=''
			elif current_url=='languages':
				get_paradigm_value=Paradigm.objects.all()
				get_language_value=''
			elif current_url=='programmers':
				get_language_value=Language.objects.all()
				get_paradigm_value=''
			return render(request,'apipage.html',{
				"get_firstname":data_values[0].title(),
				"get_lastname":data_values[1].title(),
				"current_date":data_values[3],
				"user_id":data_values[2],
				"current_url":current_url,
				"get_paradigm_value":get_paradigm_value,
				"get_language_value":get_language_value
				
			})
		else:
			 return redirect('http://'+request.get_host()+'/')
	except Exception as ex:
			log.error('Error in json data: %s' % ex)


@csrf_exempt
def ApiSubmitData(request):
	result={}
	try:
		req_data=request.POST.get('data')
		req_data=json.loads(req_data)
		log.info(req_data)

	except Exception as ex:
		log.error("Error in redirecting page: %s" % ex)
	try:
		if request.method=="POST":
			if UserRegisterationDetails.objects.filter(id=req_data['user_id']).exists():
				get_user_id_instance=UserRegisterationDetails.objects.get(id=req_data['user_id'])
				if req_data['current_url']=='paradigms':
					Paradigm.objects.create(register_user_id=get_user_id_instance,
						name=req_data['name_api'])
					get_paradigm_objects=Paradigm.objects.filter().last()
					name_data=get_paradigm_objects.name
					id=get_paradigm_objects.id
					url=domain_name+req_data['current_url']+"/"+str(id)+"/"
					result_set={"id":id, "url":url, "name":name_data}

				elif req_data['current_url']=='languages':
					get_paradigm_instance=Paradigm.objects.get(id=req_data['paradigm_value'])
					Language.objects.create(register_user_id=get_user_id_instance,
						name=req_data['name_api'],
						paradigm=get_paradigm_instance).save()
					get_language_objects=Language.objects.filter().last()
					name_data=get_language_objects.name
					id=get_language_objects.id
					url=domain_name+req_data['current_url']+"/"+str(id)+"/"
					paradigm_id=get_language_objects.paradigm.id
					paradigm=domain_name+"paradigms/"+str(paradigm_id)+"/"
					result_set={"id":id, "url":url, "name":name_data,"paradigm":paradigm}


				elif req_data['current_url']=='programmers':
					language_Array=[]
					language_Set=''
					
					get_language_value=req_data['language_value']
					for language_id in get_language_value:
						language_id_set=language_id
						if language_Set=='':
							language_Set=language_id_set
						else:
							language_Set=language_Set+"|"+language_id_set

						language=domain_name+"languages/"+str(language_id_set)+"/"
						language_Array.append(language)
					
					
					Programmer.objects.create(register_user_id=get_user_id_instance,
						name=req_data['name_api'],
						languages=language_Set
						).save()
					

					get_programmer_objects=Programmer.objects.filter().last()
					name_data=get_programmer_objects.name
					id=get_programmer_objects.id
					url=domain_name+req_data['current_url']+"/"+str(id)+"/"
					
					result_set={"id":id, "url":url, "name":name_data,"languages":language_Array}
					
				result['result_set']=result_set
				result['a_res']='1'
			else:
				result['a_res']='0'

			
					
		return HttpResponse(json.dumps(result), content_type='application/json')
	except Exception as ex:
		log.error("Error in redirecting page: %s" % ex)

@csrf_exempt
def get_api_list_view_data(request,data):


	result={}
	try:
		req_data=json.loads(data)
		
	except Exception as ex:
			log.error('Error in json data: %s' % ex)
	try:
		if request.method=='POST':
			apiViewData=[]
			
			if req_data['url_name']=='paradigms':
				if Paradigm.objects.filter(register_user_id=req_data['user_id']).exists():
					get_paradigm_object=Paradigm.objects.filter(register_user_id=req_data['user_id'])
		
					for i in get_paradigm_object:
						
						url=domain_name+req_data['url_name']+"/"+str(i.id)+"/"
						result_set={"id":i.id, "url":url, "name":i.name}
						apiViewData.append(result_set)
				else:
					apiViewData=apiViewData

			elif req_data['url_name']=='languages':
				if Language.objects.filter(register_user_id=req_data['user_id']).exists():
					get_language_object=Language.objects.filter(register_user_id=req_data['user_id'])
		
					for i in get_language_object:
						
						url=domain_name+req_data['url_name']+"/"+str(i.id)+"/"
						paradigm_id=i.paradigm.id
						paradigm=domain_name+'paradigm/'+str(paradigm_id)+"/"
						result_set={"id":i.id, "url":url, "name":i.name,"paradigm":paradigm}
						apiViewData.append(result_set)
				else:
					apiViewData=apiViewData
			elif req_data['url_name']=='programmers':
				if Programmer.objects.filter(register_user_id=req_data['user_id']).exists():
					get_programmer_object=Programmer.objects.filter(register_user_id=req_data['user_id'])
					
					for i in get_programmer_object:
						
						url=domain_name+req_data['url_name']+"/"+str(i.id)+"/"
						language_obj=i.languages.split("|")
						log.info(language_obj)
						language_Array=[]
						language_Set=''
						for language_id in language_obj:
							language_id_set=language_id
							if language_Set=='':
								language_Set=language_id_set
							else:
								language_Set=language_Set+"|"+language_id_set

							language=domain_name+"languages/"+str(language_id_set)+"/"
							language_Array.append(language)

						# paradigm=domain_name+'paradigm/'+str(paradigm_id)+"/"
						result_set={"id":i.id, "url":url, "name":i.name,"languages":language_Array}
						apiViewData.append(result_set)
				else:
					apiViewData=apiViewData

			result['apiViewData']=apiViewData
		return HttpResponse(json.dumps(result), content_type='application/json')
	except Exception as ex:
		log.error("Error in redirecting page: %s" % ex)