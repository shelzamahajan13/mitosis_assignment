var check_question_answer_result;
var next_question_answer_set_result;
var finish_test_report_set_result;
var dashboard_redirect_value;

function check_question_answer(){
	
	check_question_answer_result()
}

function next_question_answer_set(){
	next_question_answer_set_result()
}

function finish_test_report_set(){
	finish_test_report_set_result()
}

function wb_openNav() {
    
    document.getElementById("wb_myNav").style.height = "100%";
   
}

function dashboard_redirect(){
	
	dashboard_redirect_value()
}

function wb_closeNav() {
    
    document.getElementById("wb_myNav").style.height = "0%";
    $('.wb_overlay-content').empty();
    $('.wb_overlay-content').css("display", "block");

}

function openNav() {
 $('#mySidebar').show();
 $('.overlay_sec').addClass('animated slideInRight');
 setTimeout(function(){ $('.overlay_sec').removeClass('animated slideInRight'); }, 1000);

 }
 function closeNav() {
 
 $('.overlay_sec').addClass('animated slideOutRight');
  setTimeout(function(){  
  $('#mySidebar').hide();
  $('.overlay_sec').removeClass('animated slideOutRight');
  }, 1000);
}




app.controller("dashboard_data_controller",function($scope,$http,CONFIG,$timeout,$interval){
	var BASE_URL=JSON.stringify(CONFIG['BASE_URL']).replace(/\"/g, "");



	var tick = function() {
		var current_date=new Date();
	    $scope.clock =current_date;
	    var get_hours=current_date.getHours();
	    var TimeOut_Thread = $timeout(function(){ $scope.date_time_value_function(get_hours) } , 100);
	}
	tick();
	$interval(tick, 500);

	$scope.date_time_value_function=function(get_hours){
		
		if($scope.user.dy_user_id!=''){
    		var data=JSON.stringify({"get_hours":get_hours})
    		
    		$http.post(BASE_URL+"date_time_value_function/"+data).then(function(response){
    			$scope.greeting_name=JSON.stringify(response.data.greeting_name).replace(/\"/g, "")
    			
    		},function(response){
    			console.log("error")
    		})
    		
    	}
	}

	$scope.submit_api_call_data=function(user,current_url){
		
		if(current_url=='paradigms'){
			var name=$scope.user.name_api
			if(name!=''){
				result=true;
			}else{
				result=false
			}
		}else if(current_url=='languages'){
			
			var name=$scope.user.name_api
			var paradigm=$scope.user.paradigm_value
			if(name!='' && paradigm!='0'){
				result=true
			}else{
				result=false
			}
		}else if(current_url=='programmers'){
			var name=$scope.user.name_api
			var languages=$scope.user.language_value
			if(name!='' && languages!='0'){
				result=true
			}else{
				result=false
			}
		}


		if(result==true){
			var data=angular.copy(user)
			var data=JSON.stringify(data)
			$(".loader").show()
			$.ajax({
				url:'/api_submit_data/',
				type:'POST',
				dataType: 'json',
				data:{'data':data},
				success: function(response){
					$(".loader").hide()
					var a=response['a_res'];
					
					if(a=="1"){
						
						var json_data=JSON.stringify(response['result_set'])
						$(".data_display").text(json_data)
				       
			  		 	$(".empty_value").val('')
			  		 	$("#sel1").val('0')
					}
					
					if(a=='0'){
						
						$.alert({
				            icon: 'fa fa-exclamation-circle',
				            theme: 'white',
				            animation: 'scale',
				            type: 'red',
				            title: 'Error!',
				            content: 'Server Error Occured',
				        })
					}
				},function (response) {
					if(response.status=="500"){
						console.log('wrong1'); 
					}
				}
			})
		}
	}

	$scope.get_api_list_view_data=function(url){
		var data=JSON.stringify({"url_name":url,"user_id":$scope.user.user_id})
		$http.post(BASE_URL+"get_api_list_view_data/"+data).then(function(response){
			var json_data=JSON.stringify(response.data['apiViewData'])
			$(".data_display").text(json_data)
		},function(response){
			console.log("error")
		})
	}
	
})