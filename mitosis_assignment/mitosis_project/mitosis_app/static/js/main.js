
var app = angular.module('myApp', ['ui.utils.masks','naif.base64']);

app.config(function ($interpolateProvider) {


    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');

});



domainName = document.location.origin;
console.log(domainName)

app.constant('CONFIG', {
    'APP_NAME' : 'My Awesome App',
    'APP_VERSION' : '0.0.0',
    'GOOGLE_ANALYTICS_ID' : '',
    'BASE_URL' : domainName+'/',
    'SYSTEM_LANGUAGE' : '',
    'FILE_PATH':domainName+'/static/img/uploads/'
})

$(window).on("load", function () {
     $(".loader").addClass('animated heartBeat slow infinite ').fadeOut('slow');
});

app.directive('customFilterValidation', function () {
      return {
        require: 'ngModel',
        link: function(scope, element, attr, ngModelCtrl) {
          function fromUser(text) {
            var transformedInput = text.replace(/\s/g, '');
            
            if (transformedInput !== text) {
              ngModelCtrl.$setViewValue(transformedInput);
              ngModelCtrl.$render();
            }
            return transformedInput;
          }
          ngModelCtrl.$parsers.push(fromUser);
        }
      };
});






