from django.db import models
from django.utils import timezone
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class UserRegisterationDetails(models.Model):
	auth_user = models.ForeignKey(User, on_delete=models.CASCADE)
	first_name=models.CharField(max_length=200,null=True,blank=True)
	last_name=models.CharField(max_length=50,null=True,blank=True)
	email_mobile=models.CharField(max_length=100, null=True,blank=True)
	password=models.CharField(max_length=100,null=True,blank=True)
	user_registered_date=models.DateTimeField('user_registered_date',null=True,blank=True)

class Paradigm(models.Model):
	register_user_id=models.ForeignKey(UserRegisterationDetails, on_delete=models.CASCADE)
	name=models.CharField(max_length=100,null=True,blank=True)

class Language(models.Model):
	register_user_id=models.ForeignKey(UserRegisterationDetails, on_delete=models.CASCADE)
	paradigm=models.ForeignKey(Paradigm, on_delete=models.CASCADE)
	name=models.CharField(max_length=100,null=True,blank=True)

class Programmer(models.Model):
	register_user_id=models.ForeignKey(UserRegisterationDetails, on_delete=models.CASCADE)
	name=models.CharField(max_length=100,null=True,blank=True)
	languages=models.CharField(max_length=200,null=True,blank=True)
